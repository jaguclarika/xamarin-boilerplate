﻿namespace ProjectName.Forms.UI
{
    using System.Threading.Tasks;
    using ProjectName.Core.Application;
    using ProjectName.Core.Navigation;
    using ProjectName.Core.ViewModel;
    using Xamarin.Forms;

    public partial class App : Application
    {
        public App(ICoreNavigationController coreNavigationController)
        {
            this.InitializeComponent();

            this.MainPage = new FormsPage();

            // Complex navigation use this controller to get information and create a flow : coreNavigationController.OpenFirstPage(this);
        }

        #region builder
        public static Application Build()
        {
            return new App(new BaseCoreNavigationController());
        }
        #endregion

        #region App lifecicle

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }
        #endregion
    }
}
