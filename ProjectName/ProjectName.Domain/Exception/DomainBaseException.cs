﻿namespace ProjectName.Domain.Exception
{
    using System;

    /// <summary>
    /// Domain base exception.
    /// </summary>
    public class DomainBaseException : Exception
    {
    }
}
