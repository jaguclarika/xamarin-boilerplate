﻿namespace ProjectName.Domain.UseCase
{
    using System;

    /// <summary>
    /// Base use case.
    /// </summary>
    public interface IBaseUseCase
    {
    }
}
