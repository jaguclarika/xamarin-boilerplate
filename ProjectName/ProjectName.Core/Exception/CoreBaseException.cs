﻿namespace ProjectName.Core.Exception
{
    using System;

    /// <summary>
    /// Core base exception.
    /// </summary>
    public class CoreBaseException : Exception
    {
    }
}
