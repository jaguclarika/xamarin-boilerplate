﻿namespace ProjectName.Core.Application
{
    using System.Threading.Tasks;
    using ProjectName.Core.ViewModel;

    /// <summary>
    /// Core app in this interfaces only add the principals behaviours for the app.
    /// </summary>
    public interface ICoreApp
    {
        /// <summary>
        /// Registers the first page.
        /// </summary>
        /// <returns>The first page.</returns>
        /// <typeparam name="TViewModel">The 1st type parameter.</typeparam>
        Task RegisterFirstPage<TViewModel>() where TViewModel : ICoreBaseViewModel;
    }
}
