﻿namespace ProjectName.Core.Navigation
{
    using System;
    using System.Threading.Tasks;
    using ProjectName.Core.Application;
    using ProjectName.Core.ViewModel;

    /// <summary>
    /// Base core navigation controller.
    /// </summary>
    public class BaseCoreNavigationController : ICoreNavigationController
    {
        /// <summary>
        /// Back the specified viewModel.
        /// </summary>
        /// <returns>Task reference.</returns>
        /// <param name="viewModel">View model to close.</param>
        public Task Back(ICoreBaseViewModel viewModel)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Opens the first page.
        /// </summary>
        /// <returns>Task reference.</returns>
        /// <param name="coreApp">Core app reference with custom register firstpage.</param>
        public Task OpenFirstPage(ICoreApp coreApp)
        {
            return coreApp.RegisterFirstPage<ICoreBaseViewModel>();
        }

        /// <summary>
        /// Res the open app.
        /// </summary>
        /// <returns>Task reference.</returns>
        public Task ReOpenApp()
        {
            throw new NotImplementedException();
        }
    }
}
