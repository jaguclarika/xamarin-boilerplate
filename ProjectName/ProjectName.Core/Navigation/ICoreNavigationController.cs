﻿namespace ProjectName.Core.Navigation
{
    using System.Threading.Tasks;
    using ProjectName.Core.Application;
    using ProjectName.Core.ViewModel;

    /// <summary>
    /// Core navigation controller.
    /// </summary>
    public interface ICoreNavigationController
    {
        /// <summary>
        /// Opens the first page when application is running.
        /// </summary>
        /// <returns>The first page.</returns>
        /// <param name="coreApp">Core app reference.</param>
        Task OpenFirstPage(ICoreApp coreApp);

        /// <summary>
        /// Reopen the app with the first page and clear the stack.
        /// </summary>
        /// <returns>Task reference.</returns>
        Task ReOpenApp();

        /// <summary>
        /// Close the specified viewModel.
        /// </summary>
        /// <returns>Task reference.</returns>
        /// <param name="viewModel">View model refernce.</param>
        Task Back(ICoreBaseViewModel viewModel);
    }
}
