﻿namespace ProjectName.Forms.Droid
{
    using Android.App;
    using Android.Content.PM;
    using Android.OS;
    using ProjectName.Forms.UI;
    using Xamarin.Forms.Platform.Android;

    [Activity(Label = "ProjectName.Forms.Droid",
              Icon = "@drawable/icon",
              Theme = "@style/MyTheme",
              MainLauncher = true,
              ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : FormsAppCompatActivity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            FormsAppCompatActivity.TabLayoutResource = Resource.Layout.Tabbar;
            FormsAppCompatActivity.ToolbarResource = Resource.Layout.Toolbar;

            base.OnCreate(savedInstanceState);

            global::Xamarin.Forms.Forms.Init(this, savedInstanceState);

            this.LoadApplication(App.Build());
        }
    }
}