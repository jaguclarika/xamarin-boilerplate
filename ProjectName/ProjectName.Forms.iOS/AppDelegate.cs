﻿namespace ProjectName.Forms.IOS
{
    using Foundation;
    using ProjectName.Forms.UI;
    using UIKit;
    using Xamarin.Forms.Platform.iOS;

    [Register("AppDelegate")]
    public class AppDelegate
        : FormsApplicationDelegate
    {
        public override bool FinishedLaunching(UIApplication uiApplication, NSDictionary launchOptions)
        {
            global::Xamarin.Forms.Forms.Init();

            // Code for starting up the Xamarin Test Cloud Agent
#if DEBUG
            Xamarin.Calabash.Start();
#endif

            this.LoadApplication(App.Build());

            return base.FinishedLaunching(uiApplication, launchOptions);
        }
    }
}
