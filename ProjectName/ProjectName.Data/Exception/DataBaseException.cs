﻿namespace ProjectName.Data.Exception
{
    using System;

    /// <summary>
    /// Data base exception.
    /// </summary>
    public class DataBaseException : Exception
    {
    }
}
