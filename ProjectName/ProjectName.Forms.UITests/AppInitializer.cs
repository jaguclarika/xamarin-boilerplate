﻿namespace ProjectName.Forms.UITests
{
    using System;
    using System.IO;
    using System.Linq;
    using Xamarin.UITest;
    using Xamarin.UITest.Queries;

    public static class AppInitializer
    {
        public static IApp StartApp(Platform platform)
        {
            if (platform == Platform.Android)
            {
                return ConfigureApp.Android.StartApp();
            }

            return ConfigureApp.iOS.StartApp();
        }
    }
}
