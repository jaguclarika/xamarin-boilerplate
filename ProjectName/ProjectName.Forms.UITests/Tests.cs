﻿namespace ProjectName.Forms.UITests
{
    using System.Linq;
    using NUnit.Framework;
    using Xamarin.UITest;
    using Xamarin.UITest.Queries;

    [TestFixture(Platform.Android)]
    [TestFixture(Platform.iOS)]
    public class Tests
    {
        private IApp app;
        private Platform platform;

        public Tests(Platform platform)
        {
            this.platform = platform;
        }

        [SetUp]
        public void BeforeEachTest()
        {
            this.app = AppInitializer.StartApp(this.platform);
        }

        [Test]
        public void WelcomeTextIsDisplayed()
        {
            var results = this.app.WaitForElement(c => c.Marked("Welcome to Xamarin Clarika!"));
            this.app.Screenshot("Welcome screen.");

            Assert.IsTrue(results.Any());
        }
    }
}
