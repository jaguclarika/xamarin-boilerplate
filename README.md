
### Build Status:

* Master: [![Build status](https://build.appcenter.ms/v0.1/apps/91f56079-9661-47c6-9ba0-0a79a572c242/branches/master/badge)](https://appcenter.ms)
* Develop: [![Build status](https://build.appcenter.ms/v0.1/apps/91f56079-9661-47c6-9ba0-0a79a572c242/branches/develop/badge)](https://appcenter.ms)
* MvvmCrossIntegration: [![Build status](https://build.appcenter.ms/v0.1/apps/91f56079-9661-47c6-9ba0-0a79a572c242/branches/feature%2Fmvvmcross_integration/badge)](https://appcenter.ms)

### If you want use MvvmCross boilerplate [click me!](https://bitbucket.org/jaguclarika/xamarin-boilerplate/src/663e1593c6d77186ff95a79372c86eb64674dff2/README.md?at=feature%2Fmvvmcross_integration&fileviewer=file-view-default)

# Xamarin Forms Boilerplate
---
Structure solution app that Clarika use for new Xamarin.Forms projects. 

## Requirements
---
* [Android SDK](https://developer.android.com/studio/index.html?hl=es-419)
* [Xamarin SDK](https://open.xamarin.com/)
* [Visual Studio 2017 or later](https://www.visualstudio.com/es/vs/mac/)
* [XCode](https://developer.apple.com/xcode/)
* [Git](https://git-scm.com/)

## Architecture
---
This architecture that are based on [MVVM](https://en.wikipedia.org/wiki/Model%E2%80%93view%E2%80%93viewmodel) (Model View ViewModel), and [Clean Architecture](https://8thlight.com/blog/uncle-bob/2012/08/13/the-clean-architecture.html)

![Diagram](./ArchitectureResources/architecture.jpg)



##### Description:

* **Dependency Rule**: That source code dependencies can only point inwards. Nothing to the right of diagram can know anything at all about something to the left.
* **Data**: Contains the acces to repositories and datasources (like database, api calls, cache, etc). Also include the mappers to transform an entity to a model class.
* **Domain**: constains the business model and the use cases.
* **Core**: This contains the viewmodel and core definition using C# and Xamarin that exclude Xamarin.Forms
* **UI:** This layer contains 3 projects:

    * Forms.UI: Contains the shared views that use Xamarin.Froms
    * Forms.iOS: Contains iOS platform application and features using Xamarin.Forms.iOS.
    * Forms.Android: Contains Android platform application and features using Xamarin.Forms.Droid.

### Code quality
---
The project may or may not include unit test, functional test. Depending on requirements and project size. If include Unit test and functional, create one project per project testing.

#### Code quality rules:
1. Program to interfaces not implementations.
2. Use [Dependency injection](https://en.wikipedia.org/wiki/Dependency_injection).
3. Handling exceptions by layer.
 
### Code Analysis tools
---
The following code analysis tools are set up on this project:

* [StyleCop](https://github.com/StyleCop/StyleCop): Analyzes C# source code to enforce a set of style and consistency [rules](http://stylecop.soyuz5.com/StyleCop%20Rules.html)

### Performance
---
 * [Xamarin.Forms Performance](https://docs.microsoft.com/en-us/xamarin/xamarin-forms/deploy-test/performance)
 * [Cross Platform Performance](https://docs.microsoft.com/en-us/xamarin/cross-platform/deploy-test/memory-perf-best-practices)
 * [List View Performance](https://docs.microsoft.com/en-us/xamarin/xamarin-forms/user-interface/listview/performance)
 
  
## Git Flow
---
The Recommended flow is [GitFlow](https://danielkummer.github.io/git-flow-cheatsheet/)

![GitFlow Diagram](./ArchitectureResources/gitflow.png)

## New project setup
---
To quickly start a new project from this boilerplate follow the next steps:

   1. Download this [repository as zip](https://bitbucket.org/jaguclarika/xamarin-boilerplate/get/82638446d019.zip) 
   2. Change the solution and project name and namespaces.

      * Rename `solution`: Replace {ProjectName} with your Project Name
      * Rename `project`: Replace {ProjectName} with your ProjectName
      * Rename `namespaces` in all projects.
      * Rename `applicationName` & `packageName` in AndroidManifest.xml (Droid)
      * Rename `applicationName` & `BundleIdentifier` in Info.plist (iOS)
      
  3. Create a new git repository
  4. Update `README` with information relevant to the new project.
  5. Update `LICENSE` to match the requirements of the new project.

## Recommended libraries:
---

* [MVVMCross](https://github.com/MvvmCross/MvvmCross) 
* [FFImageLoading](https://github.com/luberda-molinet/FFImageLoading) 
* [AutoMapper](https://github.com/AutoMapper/AutoMapper) 
* [SQLite Net](https://github.com/oysteinkrog/SQLite.Net-PCL) 
* [Newtonsoft](https://github.com/JamesNK/Newtonsoft.Json) 

## License
----
```
    Copyright 2018 Clarika Ltd.

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
```
